#!/bin/bash

# Получаем список
installed_packages=$(dpkg-query -W -f='${Package} ${Version}\n')

# Создаем массив для хранения приложений и их версий
declare -A apps

# Обрабатываем строки
while read -r line; do
    # Разделяем строку на имя пакета и версию
    package=$(echo "$line" | awk '{print $1}')
    version=$(echo "$line" | awk '{print $2}')
    # Добавляем инфу в массив
    apps["$package"]=$version
done <<< "$installed_packages"

# Выводим инфу в файл
for app in "${!apps[@]}"; do
    echo "$app=${apps[$app]}"
done > installed_apps.txt
