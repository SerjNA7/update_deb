#!/bin/bash

# Создаем временный каталог для сборки
BUILD_DIR=$(mktemp -d)

# Копируем файлы приложения в каталог сборки
cp -r /path/deb/build/* "$BUILD_DIR"

# Создаем структуру каталогов для deb пакета
mkdir -p "$BUILD_DIR/DEBIAN"

# Создаем файл control для управления пакетом
cat <<EOF > "$BUILD_DIR/DEBIAN/control"
Package: ${Package}
Version: ${VERSION}
Architecture: all
Maintainer: mail@ <mail@corp.com>
Description: new pack server
EOF

# Выполняем сборку пакета
dpkg-deb --build "$BUILD_DIR" /path/to/save/* "$Pack".deb

# Удаляем временный каталог для сборки
rm -rf "$BUILD_DIR"
